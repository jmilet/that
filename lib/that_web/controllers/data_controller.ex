defmodule ThatWeb.DataController do
  use ThatWeb, :controller

  def set(conn, params) do
    key = conn |> get_resp_header("request-id")

    :poolboy.transaction(That.Rediscon, fn redis ->
      :ok = That.Rediscon.set(redis, key, Poison.encode!(params))
    end)

    json(conn, "")
  end
end
