defmodule ThatWeb.PageController do
  use ThatWeb, :controller

  def index(conn, _params) do
    render(conn, "index.html")
  end
end
