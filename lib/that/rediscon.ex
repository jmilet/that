defmodule That.Rediscon do
  use GenServer

  def start_link(_) do
    GenServer.start_link(__MODULE__, :no_state)
  end

  def set(pid, key, data) do
    GenServer.call(pid, {:set, key, data})
  end

  def get(pid, key) do
    GenServer.call(pid, {:get, key})
  end

  def init(_) do
    {:ok, redis} = Redix.start_link()
    IO.puts("Starting That.Rediscon...")
    {:ok, redis}
  end

  def handle_call({:set, key, data}, _from, redis) do
    {:ok, "OK"} = Redix.command(redis, ["SET", key, data])
    {:reply, :ok, redis}
  end

  def handle_call({:get, key}, _from, redis) do
    {:ok, data} = Redix.command(redis, ["GET", key])
    {:reply, {:ok, data}, redis}
  end
end
