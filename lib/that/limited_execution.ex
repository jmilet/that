defmodule That.LimitedExecution do
  def run(function, millis \\ 5000) do
    parent = self()

    t =
      Task.async(fn ->
        :timer.sleep(millis)
        Process.exit(parent, :expired)
      end)

    function.()
    Task.shutdown(t)
  end
end
