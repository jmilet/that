defmodule That.Application do
  use Application

  def start(_type, _args) do
    import Supervisor.Spec

    children = [supervisor(ThatWeb.Endpoint, [])] ++ create_redis_pool() ++ create_data_producers()

    opts = [strategy: :one_for_one, name: That.Supervisor]
    Supervisor.start_link(children, opts)
  end

  # Tell Phoenix to update the endpoint configuration
  # whenever the application is updated.
  def config_change(changed, _new, removed) do
    ThatWeb.Endpoint.config_change(changed, removed)
    :ok
  end

  defp create_redis_pool do
    [:poolboy.child_spec(:worker, redis_pool_config())]
  end

  defp redis_pool_config do
    [
      name: {:local, That.Rediscon},
      worker_module: That.Rediscon,
      size: 250,
      max_overflow: 2
    ]
  end

  defp create_data_producers do
    for id <- 1..4, do: Supervisor.Spec.worker(That.DataProducer, [], id: {:data_producer, id})
  end
end
