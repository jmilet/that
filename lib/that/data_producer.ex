defmodule That.DataProducer do
    use Task
    
    def start_link() do
        Task.start_link(__MODULE__, :run, [])
    end

    def run() do
        data = %{"x" => "hola", "y" => "que tal", "z" => UUID.uuid1()}
        options = [{"Content-Type", "application/json"}]
        HTTPoison.post "http://localhost:4000/api/set", Poison.encode!(data), options
        :timer.sleep(:random.uniform(2000))
        That.DataProducer.run()
    end
end