# This file is responsible for configuring your application
# and its dependencies with the aid of the Mix.Config module.
#
# This configuration file is loaded before any dependency and
# is restricted to this project.
use Mix.Config

# Configures the endpoint
config :that, ThatWeb.Endpoint,
  url: [host: "localhost"],
  secret_key_base: "PJZ8+OykttNpXKJtsPDj0iwex2uXAnTAYslg+x3K7F07W9Cw+lU4yDIL4pqJcv1L",
  render_errors: [view: ThatWeb.ErrorView, accepts: ~w(html json)],
  pubsub: [name: That.PubSub,
           adapter: Phoenix.PubSub.PG2]

# Configures Elixir's Logger
config :logger, :console,
  format: "$time $metadata[$level] $message\n",
  metadata: [:user_id]

# Import environment specific config. This must remain at the bottom
# of this file so it overrides the configuration defined above.
import_config "#{Mix.env}.exs"
