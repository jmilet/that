defmodule ThatWeb.DataControllerTest do
  use ThatWeb.ConnCase

  test "POST /api/set", %{conn: conn} do
    model = %{"c" => 100}

    conn =
      conn
      |> put_req_header("content-type", "application/json")
      |> post("/api/set", Poison.encode!(model))

    key = conn |> get_resp_header("request-id")

    {:ok, ret} =
      :poolboy.transaction(That.Rediscon, fn redis ->
        That.Rediscon.get(redis, key)
      end)

    assert ^model = Poison.decode!(ret)
  end
end
