defmodule That.LimitedExecutionTest do
  use ExUnit.Case

  test "task finishes on time" do
    That.LimitedExecution.run(fn ->
      :ok
    end)
  end

  test "task doesn't finish on time" do
    Process.flag(:trap_exit, true)

    pid =
      spawn_link(fn ->
        That.LimitedExecution.run(
          fn ->
            :timer.sleep(100)
          end,
          10
        )
      end)

    assert_receive {:EXIT, ^pid, :expired}, 3000
  end
end
