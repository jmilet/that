defmodule That.RedisconTest do
  use ExUnit.Case

  test "set get data in redis" do
    model = %{"a" => 100, "b" => 200}

    {:ok, data} =
      :poolboy.transaction(That.Rediscon, fn redis ->
        That.Rediscon.set(redis, "abc", Poison.encode!(model))
        That.Rediscon.get(redis, "abc")
      end)

    assert ^model = Poison.decode!(data)
  end
end
